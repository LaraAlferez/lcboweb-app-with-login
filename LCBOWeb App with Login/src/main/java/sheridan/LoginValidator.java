package sheridan;

public class LoginValidator {

	private static int MIN_LENGTH = 6;

	public static boolean isValidLoginName(String loginName) {
		String regex = "^[A-Za-z0-9_-]*$";
		return loginName != null && loginName.matches(regex) && loginName.length() >= MIN_LENGTH;
	}
}
