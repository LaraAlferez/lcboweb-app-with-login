package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginLengthRegular() {
		assertTrue("Invalid login", LoginValidator.isValidLoginName("laraalferez"));
	}

	@Test
	public void testIsValidLoginLengthException() {
		assertFalse("Invalid login", LoginValidator.isValidLoginName("lara"));
	}

	@Test
	public void testIsValidLoginLengthBoundaryIn() {
		assertTrue("Invalid login", LoginValidator.isValidLoginName("laraal"));
	}

	@Test
	public void testIsValidLoginLengthBoundaryOut() {
		assertFalse("Invalid login", LoginValidator.isValidLoginName("laraa"));
	}

	@Test
	public void testIsValidLoginRequiredCharactersRegular() {
		assertTrue("Invalid login", LoginValidator.isValidLoginName("laraalferez04"));
	}

	@Test
	public void testIsValidLoginRequiredCharactersException() {
		assertFalse("Invalid login", LoginValidator.isValidLoginName("laraalferez!!"));
	}

	@Test
	public void testIsValidLoginRequiredCharactersBoundaryIn() {
		assertTrue("Invalid login", LoginValidator.isValidLoginName("lara04"));
	}

	@Test
	public void testIsValidLoginRequiredCharactersBoundaryOut() {
		assertFalse("Invalid login", LoginValidator.isValidLoginName("lara!!"));
	}

}
